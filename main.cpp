#include "resolver.h"

int main() {
    Resolver mainSolver;
    mainSolver.readLibs("libs.txt");
    mainSolver.createGraph();
    mainSolver.getTargets("targets.txt");
    if (mainSolver.resolveTarget() == 1)
        return 1;
    else
        mainSolver.printSolution( std::cout );
    return 0;
}