//
// Created by Gregpack on 10-Nov-18.
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "resolver.h"
#include <sstream>

TEST_CASE ("Constructors, getters") {
    Version vers1 (1212);
    Version vers2 ("12.12");
    REQUIRE (vers1.getString() == "1212");
    REQUIRE (vers2.getString() == "12.12");
    Version vers3 = vers2;
    Dependancy dep1 ("<", vers1, "A");
    REQUIRE (dep1.getName() == "A");
    REQUIRE (dep1.getCmpr() == "<");
    REQUIRE (dep1.getVersion() == vers1);
    Library lib1("A", vers1);
    REQUIRE (lib1.getVersion() == vers1);
    REQUIRE (lib1.getName() == "A");
    Dependancy dep2 (">", vers1, "A");
    Dependancy dep3 ("<", vers1, "B");
    Dependancy dep4 ("<", vers1, "B");
    lib1.addDep(dep1);
    lib1.addDep(dep2);
    lib1.addConf(dep3);
    lib1.addConf(dep4);
    REQUIRE (lib1.getAmountOfConf() == 2);
    REQUIRE (lib1.getAmountOfDep() == 2);
}

TEST_CASE("Overloaded operators") {
    Version a1 ("1.1"), a2 ("1.2"), a3 ("1.3"), a4 ("50.5"), a5 ("1.2");
    REQUIRE (a1 < a2);
    REQUIRE (a3 > a2);
    REQUIRE (a4 > a3);
    REQUIRE (a5 == a2);
    REQUIRE (a5 >= a2);
    REQUIRE (a5 <= a2);
    Library b1 ("A", a1), b2 ("A", a2), b3 ("B", a1), b4 ("A", a5);
    REQUIRE (b2 > b1);
    REQUIRE (b3 < b2);
    REQUIRE (b1 > b3);
    REQUIRE (b4 == b2);
}

TEST_CASE("Resolver tests") {
    std :: ifstream libs;
    std :: ifstream targets;
    for (int i = 0; i < 6; i++) {
        std::stringstream output;
        std::string lib = "tests/libs" + std::to_string(i) + ".txt";
        std::string target = "tests/targets" + std::to_string(i) + ".txt";
        Resolver mainSolver;
        mainSolver.readLibs(lib);
        mainSolver.createGraph();
        mainSolver.getTargets(target);
        mainSolver.resolveTarget();
        mainSolver.printSolution(output);
        switch (i) {
            case 0 :
                REQUIRE (output.str() == "A 1.0\nB 1.1\n");
                break;
            case 1 :
                REQUIRE (output.str() == "A 2.0\n");
                break;
            case 2 :
                REQUIRE (output.str() == "A 1.0\nB 2.0\n");
                break;
            case 3 :
                REQUIRE (output.str() == "A 1.0\nB 2.0\n");
                break;
            case 4 :
                REQUIRE (output.str() == "A 1.5\n");
                break;
            case 5 :
                REQUIRE (output.str() == "A 1.1\nB 1.1\nC 3.0\n");
                break;
            case 6 :
                REQUIRE (output.str() == "A 1.1\nB 1.1\nD 1.5\n");
                break;
        }
    }

}