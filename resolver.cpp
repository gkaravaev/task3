//
// Created by Gregpack on 21-Oct-18.
//

#include "resolver.h"

bool Version::operator==(const Version &check) const {
    return version == check.version;
}

bool Version::operator>(const Version &arg) const {
    int size = version.size() < arg.version.size() ? arg.version.size() : version.size();
    int temp1 = 0, temp2 = 0;
    for (int i = 0; i < size; i++) {
        if (i >= version.size()) temp1 = 0;
        else temp1 = version[i];
        if (i >= arg.version.size()) temp2 = 0;
        else temp2 = arg.version[i];
        if (temp1 > temp2) return true;
        else if (temp1 < temp2) return false;
        else continue;
    }
    return false;
}

bool Version::operator<(const Version &arg) const {
    int size = version.size() < arg.version.size() ? arg.version.size() : version.size();
    int temp1 = 0, temp2 = 0;
    for (int i = 0; i < size; i++) {
        if (i >= version.size()) temp1 = 0;
        else temp1 = version[i];
        if (i >= arg.version.size()) temp2 = 0;
        else temp2 = arg.version[i];
        if (temp1 < temp2) return true;
        else if (temp1 > temp2) return false;
        else continue;
    }
    return false;
}

Version::Version(const std::string &vers) {
    int temp = 0;
    for (char ver : vers) {
        if (isdigit(ver)) {
            temp *= 10;
            temp = temp + (ver - '0');
        }
        else {
            version.push_back(temp);
            temp = 0;
        }
    }
    version.push_back(temp);
}

Version::Version(int vers) {
    version.push_back(vers);
}

bool Version::operator<=(const Version &arg) const {
    return (*this) < arg || (*this) == arg;
}

bool Version::operator>=(const Version &arg) const {
    return (*this) > arg || (*this) == arg;
}

bool Version::operator!=(const Version &arg) const {
    return !((*this) == arg);
}

Version &Version::operator=(const Version &vers) {
    version = vers.version;
    return *this;
}

std::string Version::getString() {
    std::string vers;
    for (int i = 0; i < version.size(); i++) {
        vers = vers + std::to_string(version[i]);
        vers = vers + '.';
    }
    vers.resize(vers.size() - 1);
    return vers;
}

bool cmprer(Library &lib1, Library &lib2) {
    return lib1 > lib2;
}

Library::Library(const std::string &name, Version &vers) :
        name(name), vers(vers) {}

int Library::getAmountOfConf() const {
    return conflicts.size();
}

int Library::getAmountOfDep() const {
    return dependencies.size();
}

std::string Library::getName() const {
    return name;
}

Version Library::getVersion() const {
    return vers;
}

bool Library::libCmp(const std::string &cmp, Version &vers1) { //<= >= > < =
    if (cmp == "<=") return (vers < vers1 || vers == vers1);
    else if (cmp == ">=") return (vers > vers1 || vers == vers1);
    else if (cmp == ">") return (vers > vers1);
    else if (cmp == "<") return (vers < vers1);
    else if (cmp == "=") return (vers == vers1);
}

bool Library::operator<(const Library &lib) const {
    if (name == lib.getName())
        return vers < lib.getVersion();
    else
        return name > lib.getName();
}

bool Library::operator>(const Library &lib) const {
    if (name == lib.getName())
        return vers > lib.getVersion();
    else
        return name < lib.getName();
}

bool Library::operator==(const Library &lib) const {
    return (name == lib.getName() && vers == lib.getVersion());
}

bool Library::operator!=(const Library &lib) const {
    return !((*this) == lib);
}

bool Library::operator<=(const Library &lib) const {
    return ((*this) < lib || (*this) == lib);
}

bool Library::operator>=(const Library &lib) const {
    return ((*this) > lib || (*this) == lib);
}

void Library::addDep(Dependancy &dep) {
    dependencies.push_back(dep);
}

void Library::addConf(Dependancy &conf) {
    conflicts.push_back(conf);
}

void Resolver::readLibs(const std::string& filename) {
    std::ifstream libFile;
    std::string libName, vers, cmpr;
    libFile.open(filename);
    while (libFile >> libName >> vers) {
        Version ver(vers);
        Library newLib(libName, ver);
        libs.push_back(newLib);
        libFile.get(); //move to next line
        while (libFile.peek() == '+' || libFile.peek() == '-') {
            char depType;
            libFile.get(depType);
            libFile.get();
            std::string temp;
            getline(libFile, temp);
            size_t found = temp.find(' ');
            if (found == std::string::npos) {
                Version ver1("00");
                Dependancy dep("none", ver1, temp);
                switch (depType) {
                    case '+' :
                        libs.back().addDep(dep);
                        break;
                    case '-' :
                        libs.back().addConf(dep);
                        break;
                    default:
                        break;
                }
            } else {
                libName = temp.substr(0, found);
                size_t found1 = temp.find(' ', found + 1);
                cmpr = temp.substr(found + 1, found1 - found - 1);
                vers = temp.substr(found1 + 1, temp.length() - found1);
                Version verD(vers);
                Dependancy dep(cmpr, verD, libName);
                switch (depType) {
                    case '+' :
                        libs.back().addDep(dep);
                        break;
                    case '-' :
                        libs.back().addConf(dep);
                        break;
                    default:
                        break;
                }
            }
            //libFile >> libName >> cmpr >> vers;
            //libFile.get();
        }
    }
    std::sort(std::begin(libs), std::end(libs), cmprer);
}

void Resolver::createGraph() {
    for (int i = 0; i < libs.size(); i++) {
        for (int j = 0; j < libs.size(); j++) {
            graphMatrix[std::make_pair(libs[i], libs[j])] = 1; // define 1 = installable
        }
        for (int j = 0; j < libs[i].dependencies.size(); j++) {
            for (int k = 0; k < libs.size(); k++) { 
                if (libs[i].dependencies[j].getName() != libs[k].getName()) continue;
                else {
                    if (libs[i].dependencies[j].getCmpr() == "none") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = 2; //define 2 = dependancy
                        continue;
                    }
                    if (libs[i].dependencies[j].getCmpr() == "<=") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] =
                                2 * (libs[i].dependencies[j].getVersion() >= libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].dependencies[j].getCmpr() == ">=") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] =
                                2 * (libs[i].dependencies[j].getVersion() <= libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].dependencies[j].getCmpr() == "<") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] =
                                2 * (libs[i].dependencies[j].getVersion() > libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].dependencies[j].getCmpr() == ">") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] =
                                2 * (libs[i].dependencies[j].getVersion() < libs[k].getVersion());
                        continue;
                    }
                }
            }
        }
        for (int j = 0; j < libs[i].conflicts.size(); j++) {
            for (int k = 0; k < libs.size(); k++) {
                if (libs[i].conflicts[j].getName() != libs[k].getName()) continue;
                else {
                    if (libs[i].conflicts[j].getCmpr() == "none") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = 0; //define 0 = conflict
                        continue;
                    }
                    if (libs[i].conflicts[j].getCmpr() == "<=") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = (libs[i].conflicts[j].getVersion() <
                                                                         libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].conflicts[j].getCmpr() == ">=") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = (libs[i].conflicts[j].getVersion() >
                                                                         libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].conflicts[j].getCmpr() == "<") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = (libs[i].conflicts[j].getVersion() <=
                                                                         libs[k].getVersion());
                        continue;
                    }
                    if (libs[i].conflicts[j].getCmpr() == ">") {
                        graphMatrix[std::make_pair(libs[i], libs[k])] = (libs[i].conflicts[j].getVersion() >=
                                                                         libs[k].getVersion());
                        continue;
                    }
                }
            }
        }
    }
}

void Resolver::getTargets(const std::string& filename) {
    std::ifstream targetFile;
    targetFile.open(filename);
    Version temp("0");
    Dependancy target("a", temp, "a");
    std::string temp1;
    while (getline(targetFile, temp1)) {
        unsigned int found = temp1.find(' ');
        if (found == std::string::npos) {
            Version ver1("00");
            Dependancy dep("none", ver1, temp1);
            target = dep;
        } else {
            std::string libName = temp1.substr(0, found);
            unsigned int found1 = temp1.find(' ', found + 1);
            std::string cmpr = temp1.substr(found + 1, found1 - found - 1);
            std::string vers = temp1.substr(found1 + 1, temp1.length() - found1);
            Version verD(vers);
            Dependancy dep(cmpr, verD, libName);
            target = dep;
        }
        targets.push_back(target);
    }

}

bool Resolver::isSolved(Dependancy dep) {
    for (int i = 0; i < installed.size(); i++)
        if (dep.getName() == installed[i].getName()) return true;
    return false;
}

bool Resolver::resolveDependency(Library lib) {
    int solvedDependencies = 0;
    for (int i = 0; i < lib.dependencies.size(); i++) {
        if (isSolved(lib.dependencies[i])) {
            solvedDependencies++;
            continue;
        }
        for (int j = 0; j < libs.size(); j++) {
            if (libs[j].getName() == lib.dependencies[i].getName() && graphMatrix[std::make_pair(lib, libs[j])] == 2 && availableForTarget[libs[j]] == 1) {
                bool broken = false;
                for (int k = 0; k < installed.size(); k++) { 
                    if (graphMatrix[std::make_pair(libs[j], installed[k])] == 0 || graphMatrix[std::make_pair(installed[k], libs[j])] == 0) {
                        broken = true;  
                        break;
                    }
                }
                if (broken) {
                    continue; 
                } else {
                    installed.push_back(libs[j]); 
                    if (!resolveDependency(libs[j])) {  
                        installed.pop_back(); //если же зависимости не решаются то мы поставили не ту библиотеку
                        continue; //рассмотрим следующую
                    } else {
                        solvedDependencies++; //если зависимости поставленной бибилиотеки решены, то ставим значение true
                        break;
                    }
                }

            }
        }
    }
    /*if (solvedDependencies == lib.getAmountOfDep())
        return true;
    else {
        installed.pop_back();
        return false;
    }*/
    return (solvedDependencies == lib.getAmountOfDep());
}

int Resolver::resolveTarget(std::ostream & errorStream) {
    for (int i = 0; i < libs.size(); i++) {
        availableForTarget[libs[i]] = 1;
    }
    //for (int i = 0; i < targets.size(); i++)
    int i = 0, j = 0;
    while (i != targets.size())
    {
        for (j = 0; j < libs.size(); j++) {
            if (libs[j].getName() > targets[i].getName()) {
                //если библиотеки с подходящим именем кончились
                //зависимость с текущим набором не решается
                if (installed.size() == 0) {
                    errorStream << "Unable to resolve targets." << std::endl;
                    return 1;
                } else {
                    availableForTarget[installed.back()] = 0; //ставим запрет на установку последней библиотеки
                    if (i != 0)
                        while (installed.back().getName() != targets[i - 1].getName())
                            installed.pop_back();//удаляем последний таргет и все библиотеки поставленные с ним
                    installed.pop_back();
                    break;
                }
            }
            if (isTarget(libs[j], targets[i]) && (availableForTarget[libs[j]] == 1)) {
                bool broken = false; //если библиотека совпадает по имени с той которая нужная для таргета
                for (int k = 0; k < installed.size(); k++) { //проверяем её на конфликты
                    if (graphMatrix[std::make_pair(installed[k], libs[j])] == 0) {
                        broken = true;  //если они есть то сообщаем об этом
                        break;
                    }
                }
                if (broken) {
                    if (j != libs.size() - 1)
                        continue; //если конфликтует, то рассматриваем следующую
                    else {
                        if (installed.size() == 0) {
                            errorStream << "Unable to resolve targets." << std::endl;
                            return 1;
                        } else {
                            availableForTarget[installed.back()] = 0; //ставим запрет на установку последней библиотеки
                            while (installed.back().getName() != targets[i - 1].getName())
                                installed.pop_back();//удаляем последний таргет и все библиотеки поставленные с ним
                            installed.pop_back();
                            i--;
                            break;
                        }
                    }
                } else { //если нет, то ставим библиотеку и решаем для неё зависимости
                    installed.push_back(libs[j]); //ставим
                    if (!resolveDependency(libs[j])) { //решаем
                        installed.pop_back(); //если не решаются, то поставили не то
                        continue;
                    }
                    else {
                        i++;
                        break;
                    }
                }
            }
        }
    }
    return 0;
}

void Resolver::printSolution(std::ostream & out) {
    for (int i = 0; i < installed.size(); i++) {
        out << installed[i].getName() << " " << installed[i].getVersion().getString() << std::endl;
    }
}

bool Resolver::isTarget(Library lib, Dependancy dep) {
    if (lib.getName() != dep.getName())
        return false;
    else {
        if (dep.getCmpr() == "none") return true;
        if (dep.getCmpr() == ">") return dep.getVersion() < lib.getVersion();
        if (dep.getCmpr() == "<") return dep.getVersion() > lib.getVersion();
        if (dep.getCmpr() == "<=") return dep.getVersion() >= lib.getVersion();
        if (dep.getCmpr() == ">=") return dep.getVersion() <= lib.getVersion();
    }
}

Dependancy::Dependancy(const std::string &cmpr, Version &vers,
                       const std::string &name) : cmpr(cmpr),
                                                  vers(vers), name(name) {}

std::string Dependancy::getName() const {
    return name;
}

std::string Dependancy::getCmpr() const {
    return cmpr;
}

Version Dependancy::getVersion() const {
    return vers;
}

Dependancy &Dependancy::operator=(const Dependancy &dep) {
    vers = dep.getVersion();
    name = dep.getName();
    cmpr = dep.getCmpr();
    return (*this);
}
