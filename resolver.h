//
// Created by Gregpack on 21-Oct-18.
//

#ifndef RESOLVER_RESOLVER_H
#define RESOLVER_RESOLVER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <stack>


class Version {
private:
    std::vector <int> version;
public:
    std::string getString();

    Version &operator=(const Version &);

    explicit Version(const std::string &);

    explicit Version(int);

    bool operator<=(const Version &) const;

    bool operator>=(const Version &) const;

    bool operator<(const Version &) const;

    bool operator>(const Version &) const;

    bool operator==(const Version &) const;

    bool operator!=(const Version &) const;
};


class Dependancy {
private:
    std::string name;
    Version vers;
    std::string cmpr;
public:
    Dependancy &operator=(const Dependancy &);

    std::string getName() const;

    std::string getCmpr() const;

    Version getVersion() const;

    Dependancy(const std::string &, Version &, const std::string &);
};

class Library {
private:
    std::string name;
    Version vers;
    std::vector<Dependancy> dependencies;
    std::vector<Dependancy> conflicts;

public:
    friend class Resolver;

    bool operator<=(const Library &) const;

    bool operator>=(const Library &) const;

    bool operator<(const Library &) const;

    bool operator>(const Library &) const;

    bool operator==(const Library &) const;

    bool operator!=(const Library &) const;

    std::string getName() const;

    Version getVersion() const;

    bool libCmp(const std::string &, Version &);

    int getAmountOfDep() const;

    int getAmountOfConf() const;

    Library(const std::string &, Version &);

    void addDep(Dependancy &);

    void addConf(Dependancy &);
};

bool cmprer(Library &, Library &);

class Resolver {
private:
    std::map<Library, int> availableForTarget;
    std::map<std::pair<Library, Library>, int> graphMatrix;
    std::vector<Library> libs;
    std::vector<Dependancy> targets;
    std::vector<Library> installed;
public:
    void getTargets(const std::string&);

    void readLibs(const std::string&);

    void createGraph();

    bool isTarget(Library, Dependancy);

    bool resolveDependency(Library);

    int resolveTarget(std::ostream & = std::cerr);

    void printSolution(std::ostream &);

    bool isSolved(Dependancy dep);
};


#endif //RESOLVER_RESOLVER_H
